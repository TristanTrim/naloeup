import blessed
term = blessed.Terminal()

#print(f"{term.home}{term.black_on_skyblue}{term.clear}")
#print("press 'q' to quit.")

#val = term.inkey()
#import pdb
#pdb.set_trace()

print(term.clear)
for i in range(10):
  for row_num in range(term.height-1):
      with term.location(y=row_num, x=4):
          print(f'Row #{row_num+i}')
  print(term.clear_eol + 'Back to original location.',end="")
  
  from time import sleep
  sleep(1)

#with term.cbreak():
#    val = ''
#    while val.lower() != 'q':
#        val = term.inkey(timeout=3)
#        if not val:
#           print("It sure is quiet in here ...")
#        elif val.is_sequence:
#           print("got sequence: {0}.".format((val,str(val), val.name, val.code)))
#        elif val:
#           print("got {0}.".format(val))
#    print(f'bye!{term.normal}')
