
#### I hate alll of this. fuck it.

import math
import blessed
term = blessed.Terminal()

import re

ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
def removeFormatting(string):
    ## TODO: intersecting codes? that can't exist, right?
    formattings = []
    for instance in re.finditer(ansi_escape,string):
        formattings += [(instance.start(),instance.group())]
    result = ansi_escape.sub('', string)
    return(result,formattings)

def narrowByEndClipping(myList, width):
    newlist = []
    for item in myList:
        finLine = ""
        for line in item.split("\n"):
            import pdb 
            pdb.set_trace()
            line, formattings = removeFormatting(line)
            line = line[:width]
            for index,code in reversed(formattings):
                index = max(index,width)
                line = line[:index] + code + line[index:]
            finLine += line
        newlist+=[finLine]
        
    return(newlist)

def narrowByMiddleDots(myList,width):
    newlist = []
    for item in myList:
        item,formattings = removeFormatting(item)
        if len(item) > width:
            start = math.floor((width-3)/2)
            end = math.ceil((width-3)/2)
            newlist += [ f"{item[:start]}...{item[-end:]}" ]
        else:
            newlist += [ item ]
    return(newlist)

def shortenByEndClipping(myList, height):
    newlist = []
    leng = 0
    for item in myList:
        if 1+leng+item.count("\n") > height:
            return(newlist)
        leng += 1+item.count("\n")
        newlist+=[item]
    return(newlist)
        

def listBox(myList, 
        pos = None, 
        size=None,
        narrow = narrowByEndClipping,
        shorten = shortenByEndClipping,
        ):
    """
    listBox takes a list and a box and it prints the list in the box.
    """
    if pos == None:
        pos = (0,0)
    if size == None:
        size = term.width, term.height

    narrowed_list = narrow(myList,size[0])
    shortened_list = shorten(narrowed_list,size[1])
    poscur = pos[1]
    for item in shortened_list:
        if type(item) is tuple:
            item,func = item
        else:
            func = lambda x:x
        for line in item.split("\n"):
            with term.location(pos[0],poscur):
                print(func(line))
            poscur += 1


# wait fuck this strategy will butcher any blessed formatting put onto this.... fuck.
            

if __name__=="__main__":
    print(term.clear)
    listBox([("cake",term.green),"pineapple","funk","sharks"],(100,30),(6,3))
    listBox(["cake","pineapple","funk","sharks"],(100,50),(6,3),narrow=narrowByMiddleDots)
    listBox(dir(),(1,1))
    print(term.move_xy(0,term.height-4))
        
