from time import time
import profile
from contextlib import redirect_stdout, redirect_stderr
import io

def ma():
	num = 1
	for i in range(100000):
	  num+=num/1234324
	return(num)

def do():
	num = ma()
	#print(num)

def test():
	foo = io.StringIO()
	with redirect_stdout(foo),redirect_stderr(foo):
		t1 = time()
		profile.run("do()")
	t2 = time()
	do()
	t3 = time()
	return(t2-t1,t3-t2)

for a in range(10,500,10):
	p = 0
	w = 0
	for _ in range(a):
		pp,ww = test()
		p += pp
		w += ww

	print(a,": prof",
		str(p/a)[:8],"without",
		str(w/a)[:8])
	print( "="*int(10000*p/a))
	print( "."*int(10000*w/a))
#print(f"with prof {t2-t1} without {t3-t2}")
