from time import sleep
import pkgutil

import blessed

term = blessed.Terminal()


with term.hidden_cursor(), term.cbreak(), term.location():
    print(term.clear)
    print(term.move_xy(term.width//2, term.height//2),end="")
    print(term.green("Welcome!"))
    sleep(.2)

class ColumnView():
    """
    why am I writing this?
    something has gone wrong inside of me, or somewhere.
    """
    def __init__(self,items):

        self.items = items

        assert not any("\n" in x for x in self.items)

        self.columnwidth = max(len(x) for x in self.items) + 3
        self.numColumns = term.width//self.columnwidth

        self.shownItems = self.items[:]
        self.filterWord = ""

        self._offset = 0

    def addToFilterWord(self,key):
        self.filterWord += key
        self.shownItems = list(item for item in self.items if item.startswith(self.filterWord))
        self.print()

    def filterWordBackspace(self):
        self.filterWord = self.filterWord[:-1]
        self.shownItems = list(item for item in self.items if item.startswith(self.filterWord))
        if self._offset >= len(self.shownItems): self._offset = len(self.shownItems) -1
        self.print()

    def offset(self,update):
        self._offset += update
        if self._offset < 0: self._offset = 0
        if self._offset >= len(self.shownItems): self._offset = len(self.shownItems) -1
        self.print()

    def print(self):
        print(term.clear)
        print(term.move_xy(0,0),self.filterWord)
        column = 0
        row = 1
        for item in self.shownItems[self._offset:]:
            print(term.move_xy(
                        column*self.columnwidth,
                        row),
                    end="")
            print(item)
            row += 1
            if row == term.height-1:
                row = 1
                column += 1
                if column == self.numColumns:
                    return

if __name__ == "__main__":

    view = ColumnView(list(x.name for x in pkgutil.iter_modules()) + [str(x)*5 for x in range(100)])
    view.print()
    val = ""
    while val != '\x1b':
        with term.cbreak():
            val = term.inkey(timeout=3)
            print("got sequence: {0}.".format((str(val), val.name, val.code)))
            if val.code == 263:
               view.filterWordBackspace()
            elif val == '\x15':#ctrl u
                view.offset(-term.height+2)
            elif val == '\x04':#ctrl d
                view.offset(term.height-2)
            #elif val == '\x1b':#escape
            elif val:
               #print("got {0}.".format(val))
               view.addToFilterWord(val)



