

# special thanks to:
# https://gist.github.com/claymcleod/b670285f334acd56ad1c

# note for windows users:
# https://pypi.org/project/windows-curses/

import sys,os
import curses
import pkgutil
from old_naloe import Naloe


def draw_menu(stdscr):

    nal = Naloe()


    k = 0
    cursor_x = 0
    cursor_y = 0

    # Clear and refresh the screen for a blank canvas
    stdscr.clear()
    stdscr.refresh()

    # Start colors in curses
    curses.start_color()
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

    # Loop where k is the last character pressed
    while nal.running:

        # Initialization
        #stdscr.clear()
        stdscr.erase()## why is this not working?
        stdscr.refresh()

        height, width = stdscr.getmaxyx()

        if k == 27: # Esc or Alt
                                        # frick everything about this
                                        # but anyway, thanks to:
                                        # https://stackoverflow.com/a/16248956/3988392
            # Don't wait for another key
            # If it was Alt then curses has already sent the other key
            # otherwise -1 is sent (Escape)
            stdscr.nodelay(True)
            n = stdscr.getch()
            if n == -1:
                # Escape was pressed
                nal.hi("esc")
            # Return to delay
            stdscr.nodelay(False)

        elif k == 0:#null
            pass
        else:
            nal.hi(k)
   #         #nalth.ex("with open('foo','w') as fl:\n\tfl.write(str(globals()))\n\tfl.write('\\n')\n\tfl.write(str(locals()))")
   #         #nalth.ex("print(locals())")

   #     elif mode == "command" and k == ord("q"):
   #         running = False
   #     elif mode == "get" and k == ord("i"):
   #         mode = "import"
   #         moduleList = list(pkgutil.iter_modules())
   #         importStr = ""

   #     elif k == curses.KEY_DOWN:
   #         cursor_y = cursor_y + 1
   #     elif k == curses.KEY_UP:
   #         cursor_y = cursor_y - 1
   #     elif k == curses.KEY_RIGHT:
   #         cursor_x = cursor_x + 1
   #     elif k == curses.KEY_LEFT:
   #         cursor_x = cursor_x - 1

        cursor_x = max(0, cursor_x)
        cursor_x = min(width-1, cursor_x)

        cursor_y = max(0, cursor_y)
        cursor_y = min(height-1, cursor_y)

        # Declaration of strings
        title = "Curses example"[:width-1]
        subtitle = "Written by Clay McLeod"[:width-1]
        keystr = "Last key pressed: {}".format(k)[:width-1]
        if k == 0:
            keystr = "No key press detected..."[:width-1]

        statusbarstr = f" {nal.mode:<13} | Press ':q' to exit | STATUS BAR | Pos: {cursor_x}, {cursor_y}"

        # Centering calculations
        start_x_title = int((width // 2) - (len(title) // 2) - len(title) % 2)
        start_x_subtitle = int((width // 2) - (len(subtitle) // 2) - len(subtitle) % 2)
        start_x_keystr = int((width // 2) - (len(keystr) // 2) - len(keystr) % 2)
        start_y = int((height // 2) - 2)

        # Rendering some text
        whstr = "Width: {}, Height: {}".format(width, height)
        stdscr.addstr(0, 0, whstr, curses.color_pair(1))

        # Render status bar
        stdscr.attron(curses.color_pair(3))
        stdscr.addstr(height-1, 0, statusbarstr)
        stdscr.addstr(height-1, len(statusbarstr), " " * (width - len(statusbarstr) - 1))
        stdscr.attroff(curses.color_pair(3))

        # Turning on attributes for title
        stdscr.attron(curses.color_pair(2))
        stdscr.attron(curses.A_BOLD)

        # Rendering title
      #  stdscr.addstr(start_y, start_x_title, title)

        # Turning off attributes for title
        stdscr.attroff(curses.color_pair(2))
        stdscr.attroff(curses.A_BOLD)

      #  # Print rest of text
      #  stdscr.addstr(start_y + 1, start_x_subtitle, subtitle)
      #  stdscr.addstr(start_y + 3, (width // 2) - 2, '-' * 4)
      #  stdscr.addstr(start_y + 5, start_x_keystr, keystr)

        if nal.mode == "import":
            stdscr.addstr(1, 0, nal.importStr+" "+str(len(nal.importStr)), curses.color_pair(1))
            stdscr.clrtoeol() # wtf, I hate curses
            list_bwop(stdscr, (x.name for x in nal.activeInput.viewing if x.name.startswith(nal.importStr)))

        stdscr.addstr(30,1,nal.repl.stdout.getvalue())

        # show / leave cursor at cursor pos
        stdscr.move(cursor_y, cursor_x)

        # Refresh the screen
        stdscr.refresh()

        # Wait for next input
        k = stdscr.getch() if nal.running else None

def list_bwop(stdscr,l):
    for i,item in enumerate(l):
        if i > 10:
            break
        stdscr.addstr(2+i,0,item)

def main():
    curses.wrapper(draw_menu)

if __name__ == "__main__":
    main()
