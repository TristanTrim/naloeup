
from collections import defaultdict
import pkgutil
#from threading import Thread
from multiprocessing import Process as Thread
import ast
from queue import SimpleQueue
import io
import sys
from contextlib import redirect_stdout
from standardcontrol import getControl

class ListInput():
    def __init__(self,items,callback):
        self.items = items
        self.viewing = items
        self.input = ""
        self.callback = callback

#class ImportInput(AbstractInputList):
#    def __init__(self):
#        super(self).__init__(self,list(pkgutil.iter_modules()))
#        self.mode = "import"
#        ### ehhhh fuck, I don't know what I'm doing here.

class Repl():
    def __init__(self):

        self.stdout = io.StringIO()
        self.globals = {}
        self.locals = {}
        # populate globals
        exec("",self.globals,self.locals)

        self.exqueue=SimpleQueue()
        self.thread=None

    def _reactor(self):
        while not self.exqueue.empty():
            code = self.exqueue.get()
#1            print("executing",code)
            compiledCode = compile(code,"","exec")
            with redirect_stdout(self.stdout):
                exec(compiledCode,self.globals,self.locals)
        self.thread=None

    def startThread(self):
#1        print("trying start thread")
        if not self.thread:
#1            print("actually starting thread")
            self.thread = Thread(target=self._reactor)
            self.thread.run()

    def ex(self,code):
        self.exqueue.put(code)
        self.startThread()

class Naloe():
    def __init__(self,control=getControl):
        self.mode = "normal"
        self.running = True
        self.repl = Repl()
        self.importStr = ""
        self.moduleList=[]

        self.control = defaultdict(dict)
        for mode,keymap in getControl(self).items():
            for key,func in keymap.items():
                if key == "esc":
                    self.control[mode]["esc"] = func
                elif key == "alphanum":
                    for kk in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890":
                        if ord(kk) not in self.control[mode]:
                            self.control[mode][ord(kk)] = func
                elif key == "backspace":
                    self.control[mode][8] = func
                elif key == "tab":
                    self.control[mode][9] = func
                elif key == "enter":
                    self.control[mode][10] = func
                else:
                    self.control[mode][ord(key)] = func

    def hi(self,keycode):
        try:
            self.control[self.mode][keycode](keycode)
        except KeyError as e:
            pass

    def sendImportKey(self,keycode=0):
        self.importStr+=chr(keycode)

    def tabcompleteImport(self,keycode=0):
        substr = list(x.name for x in self.moduleList if x.name.startswith(self.importStr))
        self.importStr = ""
        for chars in zip(*substr):
            if all(char == chars[0] for char in chars):
                self.importStr += chars[0]
            else:
                break

    def backspaceImport(self,keycode=0):
        self.importStr = self.importStr[:-1]
    def doImport(self,item):
        self.repl.ex(f"import {item}")
        self.repl.ex(f"print( {item} )")
        self.toNormalMode()
       
    # to modes 

    def toNormalMode(self,keycode=0):
        self.mode = "normal"
        self.importStr = ""

    def toGetMode(self,keycode=0):
        self.mode = "get"

    def toImportMode(self,keycode=0):
        self.activeInput = ListInput(
                list(pkgutil.iter_modules()),
                self.doImport)
        self.mode = "import"

    def toCommandMode(self,keycode=0):
        self.mode = "command"



    def printFoo(self,keycode=0):
        print("oodles of foodles")
        self.repl.ex("print('foo')")

    def quit(self,keycode=0):
        self.running=False


if __name__=="__main__":
    n = Repl()

    importpdb = ast.parse("import pdb")
    printglobalsandlocals = ast.parse("print(globals())\nprint(locals())")

    n.ex(printglobalsandlocals)
    n.ex(importpdb)
    n.ex(printglobalsandlocals)

